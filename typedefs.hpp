#pragma once
/* 
 	przejproc - custom assembler-like simulator
    Copyright (C) 2018 Rafał Wojsławowicz 
*/

#include <cstdint>
#include <string>

static const uint8_t _MEMPAGE_STACK = 0x01;


enum Adrestype {
	adrestype_implied ,
	adrestype_register ,
	adrestype_value ,
	adrestype_adres ,
	adrestype_registerBytecode ,
	adrestype_zeropage,

	adrestype_one_byte,
	adrestype_two_byte,
};










# "przejproc" Symulator wirtualnego procesora 8-bitowego

Copyright (C) 2018 Rafał Wojsławowicz 



Konwencja zapisu w tym dokumencie:

* `kod źródłowy` - oznacza, że tekst [markdown: pomiędzy znakami grawisu "\`" /pdf: specjalna czcionka] jest kodem źródłowym, oznaczeniem instrukcji, argumentem, bądź innym tekstem który należy interpretować "as-is"

* W wyniku przyjętej przeze mnie konwencji, liczby są przede wszystkim zapisywane postaci hexadecymalnej rozpoczynającej się od znaków "0x", w zapisie pożyczonym z języka C: np. 0x12 oznacza liczbę "12" w systemie hexadecymalne, która w systemie dzięsiętnym wynosi 192. Z reguły liczby są zapisywane jako 8-bitowy oktet, bądź jako 16-bitowy dwuoktet, czyli z zerami wiodącymi(np. 0x07 lub 0x0476). Są one więc przedstawiane tak, jak przedstawiłby je program pisany w kodzie C używający funkcji `printf("0x%02x", oktet);`, lub `printf("0x%02x%02x", oktet_0, oktet_1)`.





## Struktura kodu źródłowego dla programu

Kod języka wirtualnego procesora dla programu "przejproc" należy zapisać w postaci pliku tekstowego o kodowaniu ASCII. Rozszerzenie bądź nazwa pliku może być dowolna - jedyne ograniczenie może stanowić system operacyjny na którym zostaje skompilowany i wykonany program "przejproc".

Instrukcja dla wirtualnego procesora zapisywana jest w postaci listy, podobnie do powszechnego zapisu stosowanego w językach Lisp:

`(<mnemonik> <lista operandów>...)`

Gdzie:

* `<mnemonik>` oznacza mnemonik instrukcji wirtualnego procesora (wykaz dostępnych instrukcji dostępny jest przez wykonanie programu z argumentem `-isa`, co spowoduje wypisanie listy instrukcji wraz z mnemonikami i krótkim opisem). Zrzut tekstu tej komendy znajduje się w pliku "isa.txt"

* `<lista operandów>` oznacza sekwencję operandów dla danej instrukcji. Dla procesora operand zawsze stanowi stała liczbowa jedno-oktetowa, dlatego opróc oprócz wprowadzania stałych liczbowych, kompilator programu przejproc pozwala na definiowanie aliasów (nazw bardziej czytelnych dla człowieka) stałych liczbowych. Szczegóły odnośnie definiowania aliasów zapisane są w dalszym rodziale "dyrektywy"

Instrukcja rozpoczyna się otwarciem nawiasu '(', oraz kończy zamknięciem ')'. Zapis taki jednoznacznie oznacza początek i koniec instrukcji.
Poszczególne słowa rozdzielane są znakami spacji bądź tabulatora. W każdej linii tekstu kodu źródłowego może znaleźć się dowolna liczba instrukcji.
Przykładowo, można zapisać:
	
	`(inc-reg aa) (inc-reg aa) (adc-alu)`


Co dla programu jest równoznaczne  z następującym kodem:
	` (inc-reg aa)    to może być uznane jako komentarz, bo nie jest czytane przez kompilator      (inc-reg aa)
	(adc-alu)`

Instrukcja może mieć dowolną ilość operandów, teoretycznie od 0 do nieskończoności. W praktyce instrukcja ma 0, 1, lub 2 operandy. Z reguły 1 operand oznacza albo kod rejestru, albo liczbę. Natomiast 2 operandy oznaczają adres w pamięci operacyjnej.


Maszyna wirtualna rozdzielona jest na:


1. 0x100 (256) jednobajtowych rejestrów. Do rejestrów można odnosić się poprzez ich kody bajtowe (od 0x00 do 0xff). 8 pierwszych rejestrów to rejestry specjalne, o ustalonych nazwach, specjalnych funkcjach, i dwuliterowych oznaczeniach. Są to:
Rejestry ALU:
 - "aa" Akumulator główny - zapisywane w nim są wyniki operacji logicznych i matematycznych
 - "ab" Akumulator "b" - wykorzystywany jako drugi operand przy operacjach logicznych/matematycznych które wymagają dwóch operandów. np. dodawanie, odejmowanie, xor, and
Rejestry wskaźnika instrukcji. Są one modyfikowane przez instrukcje skoków, i automatycznie inkrementowanego przy każdej instrukcji czytanej z pamięci
 - "ip" strona aktualnej instrukcji
 - "ic" komórka aktualnej instrukcji
Rejestry stanu procesora.
 - "sp" wskaźnik stosu (stos znajduje się na stronie 0x01 pamięci)
 - "fl" wskaźnik flag, wykorzystywany do instrukcji skoku warunkowego oraz dodawania/odejmowania. Aktualnie wykorzystywane są dwie flagi - flaga "carry" i flaga "flag
Rejestry wskaźnika pamięci. Są one wykorzystywane do specjalnych instrukcji odczytu/zapisu do pamięci
 - "pp" strona wskazywanego bajtu
 - "pc" komórka wskazywanego bajtu
Kody bajtowe oraz krótkie opisy rejestrów nazwanych można zobaczyć po wywołaniu programu przejproc z argumentem `-help`. Zrzut tekstu z tej operacji znajduje się w pliku "help.txt"


2. 0x10000 (65536) bajtów pamięci operacyjnej. Pamięć podzielona jest na 0x100 stron, a każda strona na 0x100 komórek (bajtów). Do pamięci można odwoływać się (zapisywać, odczytywać wartości) poprzez instrukcje.






## Dyrektywy

Instrukcja, w której pierwsze słowo  zaczyna się od znaku `#` oznacza, że dana instrukcja jest dyrektywą. Dyrektywy służą przede wszystkim do generowania aliasów do adresów bądź stałych numerycznych.
	`(#adres var1)` - automatyczna definicja adresu o etykiecie "var1". Użyty później adres zostaje przekonwertowany na stałą dwubajtową.
	`(#value con1 0x55)` - definicja stałej numerycznej o etykiecie "con1" oraz wartości 0x55
	`(#start)` - definicja początku programu, czyli miejsca pierwszej instrukcji





/* 
	przejproc - custom assembler-like simulator
	Copyright (C) 2018 Rafał Wojsławowicz 
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <string>
#include <iostream>
#include <bitset>
#include <fstream>
#include <sstream>

#include "VirtualMachine.hpp"
#include "Parser.hpp"
#include "typedefs.hpp"


static bool FLAG_REALLY_VERBOSE = false;


void compileAndLoadProgram(VirtualMachine *vm, 
		std::string const &source_code )
{
	std::cout << source_code;
	uint8_t startpage = 0x20;
	uint8_t startcell = 0x00;
	
	auto parser = Parser(vm);
	
	
	std::vector<std::vector<std::string>> parsed_program = parser.parseProgram(source_code);
	
	
	if( FLAG_REALLY_VERBOSE ) {
		for (auto tokvec : parsed_program) {
			std::cout << "\n (";
			for (auto tok : tokvec) {
				std::cout << "`" << tok << "`";
			}
			std::cout << ")";
		}
	}
	
	CompiledProgram compiled_program = parser.compileParsedProgram (parsed_program, vm, startpage, startcell);
	
	std::vector<uint8_t> bytecode_program = compiled_program.bytecode_program;
	
	if( FLAG_REALLY_VERBOSE ) {
		std::cout << "\nRozmiar programu [bytes]: " << bytecode_program.size();
		for (uint8_t byte : bytecode_program) {
			printf("\n 0x%02x", byte);
		}
	}
	
	
	printf("\n Poczatek programu: 0x%02x%02x", compiled_program.start_adres.at(0) , compiled_program.start_adres.at(1));
	
	vm->cpu_state.loadVectorOfBytesToMemory(
			startpage , 
			startcell ,
			bytecode_program );
	vm->cpu_state.setRegisterValue(regcode_p, compiled_program.start_adres.at(0) );
	vm->cpu_state.setRegisterValue(regcode_c, compiled_program.start_adres.at(1) );
}



void interactiveLoop(VirtualMachine *vm)
{
	bool keep_working = true;
	int count = 0;
	std::string input;
	input.reserve(0x20);
	
	uint8_t displaymempage  = 0x00;
	uint8_t displaymemcell  = 0x00;
	uint8_t displaymemcount = 0x00;
	
	while (keep_working) {
		input.clear();
		std::cout << std::endl << "$(" << count <<"): ";
		std::getline (std::cin, input);
		
		
		if (input == "n") {
			++count;
			std::string instruction_text = vm->doMachineCycle();
			vm->cpu_state.printAdresableRegisters();
			vm->cpu_state.printStack();
			if (displaymemcount > 0) {
				std::cout << "\nPamiec:\n";
				vm->cpu_state.printMemory(displaymempage, displaymemcell, displaymemcount);
			}
			vm->cpu_state.printFetchedInstruction (instruction_text.c_str());
		} else if (input == "l") {
			vm->loopUntilInstruction(0);
			keep_working = false;
		} else if (input == "q" || input == "\0") {
			keep_working = false;
			return;
		} else if ( input.size() == 16 && input.at(0) == 'm'   ) { //`m 0x00 0x00 0x00`
			try {
				displaymempage  = (uint8_t) std::stoi(input.substr(2, 5), 0, 0);
				displaymemcell  = (uint8_t) std::stoi(input.substr(7, 10), 0, 0);
				displaymemcount  = (uint8_t) std::stoi(input.substr(12, 15), 0, 0);
				printf( "\nWyswietlanie pamieci: start @0x%02x%02x , ilosc wyswietlanych komorek: 0x%02x" , displaymempage , displaymemcell , displaymemcount );
			} catch (std::invalid_argument) {
				printf( "\n Blad: nie wiem jak zinterpretowac \"%s\" jako wartosci do wyswietlania pamieci" , input.c_str() );
			}
			if (displaymemcount > 0) {
				std::cout << "\nPamiec:\n";
				vm->cpu_state.printMemory(displaymempage, displaymemcell, displaymemcount);
			}
		} else if ( input.size() ) {
			std::cout << "Niezrozumialy symbol \"" << input << "\".\n\t`n` - nastepny cykl\n\t`q` koniec(quit)\n\t`l` - auto-loop, do konca programu\n\t`m <page> <cell> <count>` - tryb wyswietlania pamieci(e.g. `m 0x00 0x00 0x02`: @0x0000 - @0x0001)";
		}
		vm->displayScreen();
	}
	std::cout << "\nKONIEC\n";
	vm->doMachineCycle();
	vm->cpu_state.printAdresableRegisters();
	vm->cpu_state.printMemory(0x01, 0x00, 0x04);
}





void printIsa(VirtualMachine *vm)
{
	printf ("\n Dostepne instrukcje:\n");
	vm->printInstructionSet();
}


std::string loadFile (const char *filename)
{
	std::ifstream      loaded_file(filename);
	std::stringstream  buffer;
	
	
	buffer << loaded_file.rdbuf();
	std::string loaded_text = buffer.str();
	return loaded_text;
}



int main(int argc, char **argv)
{
	std::string loaded_text = std::string();
	VirtualMachine vm = VirtualMachine();
	bool text_was_loaded = false;
	
	if ( argc == 0) {
		fprintf( stderr , "uwaga: program spodziewa się przynajmniej jednego argumentu (nazwa pliku wykonywalnego jako argv[0]), ale program otrzymal 0 argumentow. Moga dziac sie rzeczy dziwne, niestworzone jesli program w ogole zadziala." );
	} else  if ( argc < 2 ) {
		printf( "\nWykorzystanie programu:" );
		printf( "\n `%s <nazwa pliku>` - skompiluj program z pliku tekstowego, nastepnie dzialaj w trybie interaktywnym", argv[0] );
		printf( "\n `%s --isa`: wyswietl dostepne instrukcje", argv[0] );
		printf( "\n `%s --help`: wyswietl podstawowe informacje o procesorze przejproc" , argv[0] );
		putchar( '\n' );
	} else  {
		if ( strcmp(argv[1], "--help") == 0 ) {
			printf ("\n Help:\n");
			std::cout << "Runtime:\n\t`n` for next cycle\n\t`q` or empty line to quit\n\t`l` to auto-loop execution\n\t`m <page> <cell> <count>` to display memory (e.g. `m 0x00 0x00 0x02`: @0x0000 - @0x0001)\n";
			vm.cpu_state.printAdresableRegistersDescription();
		} else if ( strcmp ( argv[1], "--isa") == 0 ) {
			printIsa(&vm);
		} else {
			std::cout << "Loading file" << argv[1];
			loaded_text = loadFile (argv[1]);
			text_was_loaded = true;
		}
	}
	if( !text_was_loaded ) {
		goto main_exit;
		return 0;
	}
	
	
	
	compileAndLoadProgram (&vm, loaded_text);
	puts( "Loaded" );
	interactiveLoop(&vm);
	
	
	
	
	
main_exit:
	putchar( '\n');
	putchar( '\n');
	return 0;
}

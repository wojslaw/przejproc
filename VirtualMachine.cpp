#include "VirtualMachine.hpp"
/* 
 	przejproc - custom assembler-like simulator
    Copyright (C) 2018 Rafał Wojsławowicz 
*/




void VirtualMachine::displayScreen(void)
{
	uint8_t screenpage = mempage_screen;
	static const int screen_width = 0x20; 
	
	
	char ch;
	
	putchar('\n'); putchar(' ');
	for (int i = 1; i <= screen_width; ++i) {
		putchar ('-');
	}
	putchar('\n'); 
	putchar('|');
	for (int i = 0; i <= 0xff; ++i) {
		if ( (i > 0 ) && ( i % screen_width == 0) ) {
			putchar('|');
			putchar('\n');
			putchar('|');
		}
		ch = cpu_state.getMemoryValueAt(screenpage, i);
		if( isprint(ch) ) {
			putchar(ch);
		} else {
			putchar(' ');
		}
		
	}
	
	putchar('|');
	putchar('\n'); 
	putchar(' ');
	for (int i = 1; i <= screen_width; ++i) {
		putchar ('-');
	}
}







std::string VirtualMachine::doMachineCycle(void)
{
	cpu_state.registers_instruction.clear();
	cpu_state.registers_instruction.resize(0);
	cpu_state.fetchInstructionAtProgramCounter();
	

	// execute instruction
	return executeBytecodeInstruction (cpu_state.registers_instruction.at(0));
}


WrappedInstruction VirtualMachine::getInstructionByBytecode (uint8_t bytecode)
{
	if ( bytecode > vector_of_wrapped_instructions.size()) {
		return vector_of_wrapped_instructions.at(0);
	}
	return vector_of_wrapped_instructions.at(bytecode);
}


void VirtualMachine::addWrappedInstruction (
			std::string input_mnemonic ,
			std::string input_fullname ,
			std::function<int (struct CPUState *)> input_instruction
		)
{
	vector_of_wrapped_instructions.emplace_back(WrappedInstruction(
		vector_of_wrapped_instructions.size() ,
		input_instruction ,
		input_mnemonic,
		input_fullname
	));
}

void VirtualMachine::addWrappedInstruction (
		std::string input_mnemonic , uint8_t input_argcount,
		std::string input_fullname ,
		std::function<int (struct CPUState *)> input_instruction 
		)
{
	vector_of_wrapped_instructions.emplace_back( WrappedInstruction(
		vector_of_wrapped_instructions.size() ,
		input_mnemonic ,
		input_fullname ,
		input_instruction ,
		input_argcount
	));
}

	
[[deprecated]] int VirtualMachine::loopUntilEndOfMemory ()
{
	std::cerr << __func__ << "shouldn't be called because it don't work\n";
	return 0;

	int cycles_count = 0;
	while(true) {
		doMachineCycle();
		++cycles_count;
		printf ("\n0x%02x%02x", cpu_state.registers_adresable.at(regcode_p), cpu_state.registers_adresable.at(regcode_c) );
		if ( cpu_state.registers_adresable.at(regcode_p) == 0xff 
				&& cpu_state.registers_adresable.at(regcode_c) >  0xf0  ){ 
			printf ("\n\n\nIMPORTANT!!!!!0x%02x%02x\n\n\n", cpu_state.registers_adresable.at(regcode_p), cpu_state.registers_adresable.at(regcode_c) );
			return cycles_count;
			break; 

		}
	};
	return cycles_count;
}


int VirtualMachine::loopUntilInstruction (
		uint8_t bytecode
		)
{
	int cycles_count = 0;
	do {
		doMachineCycle();
		++cycles_count;
	} while (cpu_state.registers_instruction.at(0) != bytecode);
	return cycles_count;
}







void VirtualMachine::createDefaultInstructionSet ()
{
	vector_of_wrapped_instructions.reserve(0xff);

	addWrappedInstruction( "nop", 0,
			"no operation", 
			[] (struct CPUState *cpu_state_pointer)
			{
				return 0;
			});

	//
	//----------------[ increment/decrement ]---
	//
	addWrappedInstruction( "inc-reg" , 1,
			"increment chosen register" , 
			[] (struct CPUState *cpu_state_pointer)
			{
				uint8_t operand0 = cpu_state_pointer->fetchInstructionAtProgramCounter ();
				++(cpu_state_pointer->registers_adresable.at(operand0));
				return 1;
			});


	addWrappedInstruction( "dec-reg", 1,
			"decrement chosen register", 
			[] (struct CPUState *cpu_state_pointer)
			{
				uint8_t oper0 = cpu_state_pointer->fetchInstructionAtProgramCounter ();
				--(cpu_state_pointer->registers_adresable.at(oper0));
				return 1;
			});
	

	//
	//------------------[ arithmetic, logic ]---
	//
	addWrappedInstruction( "adc-alu", 0, 
			"add with carry alu: aa = aa + ab + carry_flag; sets carry_flag when result rolls over zero", 
			[] (struct CPUState *cpu_state_pointer)
			{
				int a     = cpu_state_pointer->registers_adresable.at(regcode_a);
				int b     = cpu_state_pointer->registers_adresable.at(regcode_a);
				int carry = cpu_state_pointer->getValueOfFlag(flagnumber_carry);
				
				a = a + b + carry;
				if ( a > 0xff ) {
					cpu_state_pointer->setBitOfRegister(regcode_flags, flagnumber_carry, 1);
				} else {
					cpu_state_pointer->setBitOfRegister(regcode_flags, flagnumber_carry, 0);
				}

				cpu_state_pointer->registers_adresable.at(regcode_a) = (uint8_t) a;
				return 0;
			} );

	addWrappedInstruction( "sbc-alu", 0, 
			"subtract with carry alu: aa = aa - ab - carry_flag; sets carry_flag when result rolls over zero", [] (struct CPUState *cpu_state_pointer)
			{
				int a     = cpu_state_pointer->registers_adresable.at(regcode_a);
				int b     = cpu_state_pointer->registers_adresable.at(regcode_a);
				int carry = cpu_state_pointer->getValueOfFlag(flagnumber_carry);
				
				a = a - b - carry;
				if ( a > 0xff ) {
					cpu_state_pointer->setBitOfRegister(regcode_flags, flagnumber_carry, 1);
				} else {
					cpu_state_pointer->setBitOfRegister(regcode_flags, flagnumber_carry, 0);
				}

				cpu_state_pointer->registers_adresable.at(regcode_a) = (uint8_t) a;
				return 0;
			});

	addWrappedInstruction( "xor-alu", 0, 
			"xor-bitwise-alu", 
			[] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) xor_eq cpu_state_pointer->registers_adresable.at(regcode_b);
				return 0;
			});
	addWrappedInstruction( "or-alu", 0,
			"or-bitwise-alu", [] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) or_eq cpu_state_pointer->registers_adresable.at(regcode_b)  ;
				return 0;
			});
	addWrappedInstruction( "and-alu", 0,
			"bitwise and of both accumulators", [] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) and_eq cpu_state_pointer->registers_adresable.at(regcode_b)  ;
				return 0;
			});
	addWrappedInstruction( "not-alu", 0,
			"bitwise not main accumulator", [] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) =	compl cpu_state_pointer->registers_adresable.at(regcode_a) ;
				return 0;
			});
	addWrappedInstruction( "lsl-alu", 0,
			"logical shift left main accumulator", [] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) = 
					cpu_state_pointer->registers_adresable.at(regcode_a) << 1 ;
				return 0;
			});

	addWrappedInstruction( "lsr-alu", 0,
			"logical shift left main accumulator", [] (struct CPUState *cpu_state_pointer)
			{
				cpu_state_pointer->registers_adresable.at(regcode_a) = 
					cpu_state_pointer->registers_adresable.at(regcode_a) >> 1 ;
				return 0;
			});
			addWrappedInstruction( "swap-alu", 0,
				"swap accumulators", [] (struct CPUState *cpu_state_pointer)
			{
				uint8_t a = cpu_state_pointer->registers_adresable.at(regcode_a);
				uint8_t b = cpu_state_pointer->registers_adresable.at(regcode_b);
				cpu_state_pointer->registers_adresable.at(regcode_a) = b; 
				cpu_state_pointer->registers_adresable.at(regcode_b) = a;
				return 0;
			});


	//
	//  
	//
	addWrappedInstruction( "seta-val", 1,
			"set-a-to-value", [] (struct CPUState *cpu_state_pointer)
			{
				uint8_t operand0 = cpu_state_pointer->fetchInstructionAtProgramCounter();
				cpu_state_pointer->registers_adresable.at(regcode_a) = 
					operand0 ;
				return 1;
			});
	addWrappedInstruction( "set-pointer", 2,
			"set-pointer-to-value(2 bytes)", [] (struct CPUState *cpu_state_pointer)
			{
				uint8_t index_page = cpu_state_pointer->fetchInstructionAtProgramCounter();
				uint8_t index_cell = cpu_state_pointer->fetchInstructionAtProgramCounter();
				cpu_state_pointer->registers_adresable.at(regcode_index_page) = 
					index_page ;
				cpu_state_pointer->registers_adresable.at(regcode_index_cell) = 
					index_cell;
				return 1;
			});

	//


	//
	//------------[ jumps ]
	//
	addWrappedInstruction( "jump", 2, 
			"jump to adres", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();

		cpu_state_pointer->registers_adresable.at(regcode_p) 
			 = page;
		cpu_state_pointer->registers_adresable.at(regcode_c) 
			= cell;
		return 2;
	});
	addWrappedInstruction( "jump-if-flag", 2, 
			"if (flag == true) { jump <adres>; }", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t accumulator = cpu_state_pointer->registers_adresable.at(regcode_accumulator);
		if ( cpu_state_pointer->getValueOfFlag(flagnumber_flag)  ) { 
			cpu_state_pointer->registers_adresable.at(regcode_programcounter_page) 
				 = page;
			cpu_state_pointer->registers_adresable.at(regcode_programcounter_cell) 
				= cell;
	}
		return 2;
	});



	//=============[ flag ]===
	addWrappedInstruction( "flag-negate", 0,
			"negate condition flag", [] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->setValueOfFlag( flagnumber_flag, !(cpu_state_pointer->getValueOfFlag(flagnumber_flag)) );
		if (cpu_state_pointer->registers_adresable.at(regcode_a) == 0 ) {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, true);
		} else {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, false);
		}

		return 0;
	});
	addWrappedInstruction( "flag-if-zero", 0,
			"set condition flag is accumulator is zero", [] (struct CPUState *cpu_state_pointer)
	{
		if (cpu_state_pointer->registers_adresable.at(regcode_a) == 0 ) {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, true);
		} else {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, false);
		}

		return 0;
	});
	addWrappedInstruction( "flag-if-carry", 0,
			"set condition flag is carry flag is set", [] (struct CPUState *cpu_state_pointer)
	{
		if ( cpu_state_pointer->registers_adresable.at(regcode_a) != 0 ) {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, true);
		} else {
			cpu_state_pointer->setValueOfFlag(flagnumber_flag, false);
		}

		return 0;
	});



	//
	//-----------------[ stack ]---
	//
	addWrappedInstruction( "push-aa", 0, 
			"push acumulator onto stack", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->stackPush(cpu_state_pointer->getRegisterValue(regcode_accumulator));
		return 0;
	});
	addWrappedInstruction( "pop-aa", 0, 
			"pop to acumulator", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->setRegisterValue(
				regcode_accumulator, 
				cpu_state_pointer->stackPop());
		return 0;
	});
	addWrappedInstruction( "push-value", 1,
			"push value (1 byte) onto stack", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->stackPush(cpu_state_pointer->fetchInstructionAtProgramCounter());
		return 0;
	});
	addWrappedInstruction( "pop-reg", 1,
			"pop to chosen register", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->setRegisterValue(
				regcode_accumulator, 
				cpu_state_pointer->stackPop());
		return 0;
	});
	addWrappedInstruction( "push-reg", 1,
			"push value of register onto stack",
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->stackPush( cpu_state_pointer->fetchInstructionAtProgramCounter() );
		return 1;
	});
	addWrappedInstruction( "push-value", 1,
			"push one byte value", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->stackPush( cpu_state_pointer->fetchInstructionAtProgramCounter() );
		return 1;
	});
	addWrappedInstruction( "push-adres", 2,
			"push 2-byte (adres) value", 
			[] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->stackPush( cpu_state_pointer->fetchInstructionAtProgramCounter() );
		cpu_state_pointer->stackPush( cpu_state_pointer->fetchInstructionAtProgramCounter() );
		return 2;
	});

	// ====[ Call and return - stack-based jumps ]==
	addWrappedInstruction( "call", 2, 
			"call-procedure (push instruction counter and jump)", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();
		
		cpu_state_pointer->stackPush(cpu_state_pointer->getRegisterValue(regcode_programcounter_page));
		cpu_state_pointer->stackPush(cpu_state_pointer->getRegisterValue(regcode_programcounter_cell));

		cpu_state_pointer->registers_adresable.at(regcode_p)
			 = page;
		cpu_state_pointer->registers_adresable.at(regcode_c)
			= cell;
		return 2;
	});
	addWrappedInstruction( "call-if-flag", 2, 
			"call procedure if condition flag is set", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();
		
		cpu_state_pointer->stackPush(cpu_state_pointer->getRegisterValue(regcode_programcounter_page));
		cpu_state_pointer->stackPush(cpu_state_pointer->getRegisterValue(regcode_programcounter_cell));

		if (cpu_state_pointer->getValueOfFlag(flagnumber_flag)) {
			cpu_state_pointer->registers_adresable.at(regcode_p)
				 = page;
			cpu_state_pointer->registers_adresable.at(regcode_c)
				= cell;
		}
		return 2;
	});
	addWrappedInstruction( "return", 2, 
			"return from procedure: pops stack into instruction counter registers", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t cell = cpu_state_pointer->stackPop();
		uint8_t page = cpu_state_pointer->stackPop();
		
		cpu_state_pointer->registers_adresable.at(regcode_p)
			 = page;
		cpu_state_pointer->registers_adresable.at(regcode_c)
			= cell;
		return 2;
	});

	//
	//============[ memory ]=======
	//
	addWrappedInstruction( "save-aa", 2, 
			"save-accumulator-to-memory", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();

		cpu_state_pointer->setMemoryValueAt (
				page, cell, 
				cpu_state_pointer->registers_adresable.at (regcode_a) );
		return 2;
	});
	addWrappedInstruction( "load-aa", 2, 
			"load-accumulator-from-memory", [] (struct CPUState *cpu_state_pointer)
	{
		uint8_t page = cpu_state_pointer->fetchInstructionAtProgramCounter();
		uint8_t cell = cpu_state_pointer->fetchInstructionAtProgramCounter();
		cpu_state_pointer->registers_adresable.at (regcode_a) 
				= cpu_state_pointer->getMemoryValueAt (page, cell);
		return 2;
	});
	addWrappedInstruction( "save-index-aa", 
			"save accumulator to memory at index pointer", [] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->setMemoryValueAt (
				cpu_state_pointer->registers_adresable.at(regcode_index_page) ,
				cpu_state_pointer->registers_adresable.at(regcode_index_cell) ,
				cpu_state_pointer->registers_adresable.at (regcode_a)         );
		return 2;
	});
	addWrappedInstruction( "load-index-aa", 0, 
			"load accumulator to memory from index pointer", [] (struct CPUState *cpu_state_pointer)
	{
		cpu_state_pointer->registers_adresable.at (regcode_a) 
				= cpu_state_pointer->getMemoryValueAt (
						cpu_state_pointer->registers_adresable.at(regcode_index_page) ,
						cpu_state_pointer->registers_adresable.at(regcode_index_cell) );
		return 2;
	});


	vector_of_wrapped_instructions.shrink_to_fit();
}




VirtualMachine::VirtualMachine()
{
	createDefaultInstructionSet();
	//ctor
}


std::string VirtualMachine::executeBytecodeInstruction(uint8_t instruction_bytecode)
{
	printf ("\nExecuting instruction 0x%02x", instruction_bytecode);
	auto instruction = vector_of_wrapped_instructions.at(instruction_bytecode);
	instruction.instruction (&cpu_state);
	std::string instruction_text = instruction.mnemonic; 
	return instruction_text;
}


void VirtualMachine::printInstructionSet()
{
	printf("\nInstruction set:\n Lp.    bytecode    `mnemonic`(operandcount)    description");
	int i = 0;
	for(auto ins : vector_of_wrapped_instructions) {
		printf("\n%02d.    0x%02x    `%s`(%d):    '%s'"
				, i
				, ins.bytecode
				, ins.mnemonic.c_str()
				, ins.argcount
				, ins.fullname.c_str()
				);
		++i;
	}
}


uint8_t VirtualMachine::findInstructionByMnemonic(std::string mnemonic)
{
	return getInstructionBytecodeByMnemonic(mnemonic);
}


uint8_t VirtualMachine::getInstructionBytecodeByMnemonic(std::string mnemonic)
{
	for ( struct WrappedInstruction instr : vector_of_wrapped_instructions ) {
		if (instr.mnemonic == mnemonic) {
			return instr.bytecode;
		}
	}
	std::cout << "\nDidn't find instruction of mnemonic `" << mnemonic << "`";
	throw std::out_of_range("failed to find instruction for given mnemonic");
}


[[deprecated]] struct WrappedInstruction VirtualMachine::getInstructionByMnemonic(std::string mnemonic)
{
	for ( size_t i = 0; i < vector_of_wrapped_instructions.size(); ++i ) {
		if ( vector_of_wrapped_instructions.at(i).mnemonic == mnemonic) {
			return vector_of_wrapped_instructions.at(i);
		}
	}
	std::cout << "\nDidn't find instruction of mnemonic `" << mnemonic << "`";
	throw std::out_of_range("failed to find instruction for given mnemonic");
}



VirtualMachine::~VirtualMachine()
{

}



















void VirtualMachine::loadBytesIntoMemory(std::vector<uint8_t> vector_of_bytes, uint8_t startpage, uint8_t startcell)
{
	cpu_state.loadVectorOfBytesToMemory(startpage, startcell, vector_of_bytes);
}









void VirtualMachine::printMemory(
		uint8_t page ,
		uint8_t start_cell ,
		uint8_t cellcount 
) {
	printf("\nDeprecated functions was called: %s", __func__);
	for(uint8_t i = 0; i < cellcount; i++) {
		uint8_t curcell = start_cell+i;
		uint8_t value = *accessMemoryAt(page, curcell);
		printf("\n @$%02x%02x: $%02x   b_", page, curcell, value );
		cout << bitset<8>(value);
	}
}




uint8_t* VirtualMachine::accessMemoryAt (
		uint8_t page ,
		uint8_t cell )
{
	printf("\nDeprecated functions was called: %s", __func__);
	return 0;
}



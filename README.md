	przejproc - custom assembler-like simulator
	Copyright (C) 2018 Rafał Wojsławowicz 

# Użytkowanie

Informacje dotyczące działania programu znajdują się w pliku `./doc/poradnik.md`

Przykładowy kod dla maszyny wirtualnej przejproc znajduje się w pliku `./program.txt`


# Kompilacja:
1. Aby skompilowac program, należy mieć zainstalowane programy:
- `make`. W projekcie wykorzystany został GNU make
- `g++` lub inny kompilator języka C++, obsługujący standard C++17. W projekcie wykorzystany został kompilator z GNU Compiler Collection

Kompilacja zostaje wtedy wykonana przez wpisanie komendy `make`, będąc w katalogu z kodem programu. Można również wywołać plik shell o nazwie `test.sh` zawarty w katalogu z kodem programu.

Po wywołaniu komendy `make`, w standardowej konfiguracji program zostanie skompilowany i zlinkowany do postaci pliku wykonywalnego o nazwie `przejproc`. Tym samym program może zostać wykonany (na systemach GNU/Linux) po wykonaniu komendy `./przejproc`. Spowoduje to wyświetlenie podstawowej pomocy odnośnie

2. W przypadku braku programu `make`:

Program jest napisany w jezyku C++, zgodny ze standardem c++11, dlatego nalezy upewnic się, ze kompilator pracuje zgodnie z tym standardem. Przykladowo na moim systemie kompilacja zachodzi przez wykorzystanie komendy:

`g++ -g -std=c++11 *.cpp`

Jednak w tym przypadku należy sprawdzić, jaka jest nazwa utworzonego pliku wykonywalnego


Kompilacja oraz dzialanie zostaly przetestowana dla kompilatorow clang++ oraz g++(GNU Compiler Collection), na systemie Ubuntu w konfiguracji WSL(Windows Subsystem Linux), na systemie Windows 10. Jednak program korzysta tylko z bibliotek standardowych C oraz C++, dlatego raczej nie powinno byc problemow zwiazanych z kompilowaniem.

Testowane wersje kompilatorow:
* `g++ -v` : `gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.9)`
* `clang++ -v` : 
`clang version 3.8.0-2ubuntu4 (tags/RELEASE_380/final)
Target: x86_64-pc-linux-gnu
Thread model: posix
